import json
import datetime as dt
from dateutil import parser
import os
import glob
import csv
import re
# message,timestamp,datetime,timestamp_desc
# Belkasoft Whatsapp Message Event | Incoming Message "pnplxdoaqbnwtho" from: 35679247196@s.whatsapp.net to: msgstore.db.crypt14,
# {"time":"1633765154","event":"Whatsapp Message Sent","trigger_point":"android.database.sqlite.SQLiteDatabase","time_of_message_event":"1633765154366","evidence_object":{"text":"Kcjlcjfx","number":"35699424312","time_sent":"1633765154366","original_object":"rO0ABXNyACtjb20ud2hhdHNhcHAuam9icXVldWUuam9iLlNlbmRFMkVNZXNzYWdlSm9iAAAAAAAAAAEDABZaAAlkdXBsaWNhdGVJAAtlZGl0VmVyc2lvbkoADGV4cGlyZVRpbWVNc1oAGmluY2x1ZGVTZW5kZXJLZXlzSW5NZXNzYWdlSgAUbWVzc2FnZVNlbmRTdGFydFRpbWVaABFtdWx0aURldmljZUZhbk91dEoAEW9yaWdpbmFsVGltZXN0YW1wSQAQb3JpZ2luYXRpb25GbGFnc0kACnJldHJ5Q291bnRaACJ1c2VPbmVPbmVFbmNyeXB0aW9uT25QSGFzaE1pc21hdGNoTAAlYnJvYWRjYXN0UGFydGljaXBhbnRFcGhlbWVyYWxTZXR0aW5nc3QAE0xqYXZhL3V0aWwvSGFzaE1hcDtMABVlbmNyeXB0aW9uUmV0cnlDb3VudHNxAH4AAVsAFWVwaGVtZXJhbFNoYXJlZFNlY3JldHQAAltCTAAUZ3JvdXBQYXJ0aWNpcGFudEhhc2h0ABJMamF2YS9sYW5nL1N0cmluZztMABpncm91cFBhcnRpY2lwYW50SGFzaFRvU2VuZHEAfgADTAACaWRxAH4AA0wAA2ppZHEAfgADTAAUbGl2ZUxvY2F0aW9uRHVyYXRpb250ABNMamF2YS9sYW5nL0ludGVnZXI7TAALcGFydGljaXBhbnRxAH4AA0wAD3JlY2lwaWVudFJhd0ppZHEAfgADTAATdGFyZ2V0RGV2aWNlUmF3Smlkc3QAE0xqYXZhL3V0aWwvSGFzaFNldDtMAAx3ZWJBdHRyaWJ1dGV0AAdMWC8zODI7eHIAH29yZy53aGlzcGVyc3lzdGVtcy5qb2JxdWV1ZS5Kb2KtM/Fh6P73IwIAAUwACnBhcmFtZXRlcnN0ACtMb3JnL3doaXNwZXJzeXN0ZW1zL2pvYnF1ZXVlL0pvYlBhcmFtZXRlcnM7eHBzcgApb3JnLndoaXNwZXJzeXN0ZW1zLmpvYnF1ZXVlLkpvYlBhcmFtZXRlcnOY5eHYGN9oaQIABloADGlzUGVyc2lzdGVudEkACnJldHJ5Q291bnRaAAh3YWtlTG9ja0oAD3dha2VMb2NrVGltZW91dEwAB2dyb3VwSWRxAH4AA0wADHJlcXVpcmVtZW50c3QAEExqYXZhL3V0aWwvTGlzdDt4cAEAAABkAAAAAAAAAAAAdAAaMzU2OTk0MjQzMTJAcy53aGF0c2FwcC5uZXRzcgAUamF2YS51dGlsLkxpbmtlZExpc3QMKVNdSmCIIgMAAHhwdwQAAAACc3IAO2NvbS53aGF0c2FwcC5qb2JxdWV1ZS5yZXF1aXJlbWVudC5DaGF0Q29ubmVjdGlvblJlcXVpcmVtZW50AAAAAAAAAAECAAB4cHNyAEZjb20ud2hhdHNhcHAuam9icXVldWUucmVxdWlyZW1lbnQuQXhvbG90bE11bHRpRGV2aWNlU2Vzc2lvblJlcXVpcmVtZW507GFMP3BbXF4CAANMAAxtZXNzYWdlS2V5SWRxAH4AA0wADHJlbW90ZVJhd0ppZHEAfgADTAATdGFyZ2V0RGV2aWNlUmF3Smlkc3EAfgAFeHB0ACA3QTI5NUNBRkMwQzExNDQ5Q0JCQkY1RDQyQUVCMUQyMXQAGjM1Njk5NDI0MzEyQHMud2hhdHNhcHAubmV0c3IAEWphdmEudXRpbC5IYXNoU2V0ukSFlZa4tzQDAAB4cHcMAAAAED9AAAAAAAAAeHgAAAAAAAAAAXxpJDpqAAAAAXxj/d4+AQAAAAAAAAAAAAAAAAAAAAAAcHNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAB3CAAAABAAAAAAeHBwcHEAfgAUdAAaMzU2OTk0MjQzMTJAcy53aGF0c2FwcC5uZXRwcHBzcQB+ABZ3DAAAABA/QAAAAAAAAHh+cgAFWC4zODIAAAAAAAAAABIAAHhyAA5qYXZhLmxhbmcuRW51bQAAAAAAAAAAEgAAeHB0AAROT05FdXIAAltCrPMX+AYIVOACAAB4cAAAAAkKB0tjamxjamZ4"}}
def to_time(datetime_str):
    TIME_FORMAT="%Y-%m-%d %H:%M:%S"

    dtobj_utc = (dt.datetime.strptime(datetime_str, TIME_FORMAT) - dt.timedelta(hours=2)).replace(tzinfo=dt.timezone.utc) #2 hours
    datetime = dtobj_utc.strftime('%Y-%m-%dT%H:%M:%S')
    timestamp = dtobj_utc.timestamp()
    return[timestamp,datetime]


def parse_samsung_dumpsys(path, outfilename): #, outfile):
    outfile= open(outfilename,"w+")
    outfile.write("message,timestamp,datetime,timestamp_desc\n")
    with open(path) as dumpsys_file:
        line = dumpsys_file.readline()
        start_inst_pkg=False
        start_del_pkg=False
        start_apptimelimithistory=False
        for_time=""
        print_line=""
        while line:                       
            if len(line.strip()) > 0:        
                if "START INSTALL PACKAGE" in line:
                    start_inst_pkg=True
                    result = re.search('(.*): START INSTALL PACKAGE: observer', line)
                    for_time = result.group(1)
                    
                if ("VerificationInfo" in line) and start_inst_pkg:
                    start_inst_pkg=False

                if start_inst_pkg and "pkg{" in line:
                    result = re.search('pkg{([\._\-a-zA-Z0-9]*)}', line)
                    package = result.group(1)
                    message='Dumpsys Event: Installed package >> '+package
                    return_to_time = to_time(for_time)
                    print_line+=message+","+str(return_to_time[0])+","+str(return_to_time[1])+","+"Event time\n"

                if "START DELETE PACKAGE" in line:
                    start_del_pkg=True
                    result = re.search('(.*): START DELETE PACKAGE:', line)
                    for_time = result.group(1)
                if start_del_pkg and "pkg{" in line:
                    start_del_pkg=False
                    result = re.search('pkg{([\._\-a-zA-Z0-9]*)}', line)
                    package = result.group(1)
                    message='Dumpsys Event: Uninstalled package >> '+package
                    return_to_time = to_time(for_time)
                    print_line+=message+","+str(return_to_time[0])+","+str(return_to_time[1])+","+"Event time\n"

                if "-------- Start of AppTimeLimit History ---------" in line:
                    start_apptimelimithistory=True
                
                if "-------- End of AppTimeLimit History ---------" in line:
                    start_apptimelimithistory=False

                if start_apptimelimithistory and "-------- Start of AppTimeLimit History ---------" not in line:
                    split_line=line.split('::')
                    print(split_line)
                    time=split_line[0].strip()
                    event=split_line[1].strip()
                    message='Dumpsys Event: '+event
                    return_to_time = to_time(time)
                    print_line+=message+","+str(return_to_time[0])+","+str(return_to_time[1])+","+"Event time\n"
                    

            line = dumpsys_file.readline()
        outfile.write(print_line)
#   #275: act=android.intent.action.PACKAGE_ADDED dat=package:com.pik.pink flg=0x4000010 (has extras)
#     +22ms dispatch +37ms finish
#     enq=2021-10-09 09:44:26.340 disp=2021-10-09 09:44:26.362 fin=2021-10-09 09:44:26.399
#     extras: Bundle[{android.intent.extra.UID=10217, android.intent.extra.user_handle=0}]
#  #294: act=android.intent.action.PACKAGE_FULLY_REMOVED dat=package:com.pik.pink flg=0x5000010 (has extras)
#     +5ms dispatch 0 finish
#     enq=2021-10-09 09:44:17.604 disp=2021-10-09 09:44:17.609 fin=2021-10-09 09:44:17.609
#     extras: Bundle[{android.intent.extra.REMOVED_FOR_ALL_USERS=true, android.intent.extra.DONT_KILL_APP=false, android.intent.extra.UID=10216, android.intent.extra.DATA_REMOVED=true, android.intent.extra.user_handle=0}]

def parse_pixel_dumpsys(path, outfilename): #, outfile):
    outfile= open(outfilename,"w+")
    outfile.write("message,timestamp,datetime,timestamp_desc\n")
    with open(path) as dumpsys_file:
        line = dumpsys_file.readline()
        start_bcast=False
        print_line=""
        while line:                       
            if len(line.strip()) > 0:     
                if 'Historical broadcasts summary [background]:' in line:   
                    start_bcast=True
                if start_bcast:
                    if '#' in line:
                        message='Dumpsys Event: '+line.strip()
                    if 'enq=' in line:
                        result = re.search('enq=([\.\-:0-9 ]*) disp', line)
                        time = result.group(1)
                        return_to_time = to_time(time)
                        print_line+=message+","+str(return_to_time[0])+","+str(return_to_time[1])+","+"Event time\n"
            else:
                if start_bcast:
                    start_bcast=False
                    

            line = dumpsys_file.readline()
        outfile.write(print_line)





# file_name='samsungs21'
# parse_samsung_dumpsys("/mnt/2TBdrive/test/dumpsys_parser/"+file_name+"_dumpsys.txt","/mnt/2TBdrive/test/dumpsys_parser/"+file_name+"_dumpsys_timesketch.csv") #, outfile)
file_name='nexus5'
parse_pixel_dumpsys("/mnt/2TBdrive/test/dumpsys_parser/"+file_name+"_dumpsys.txt","/mnt/2TBdrive/test/dumpsys_parser/"+file_name+"_dumpsys_timesketch.csv") #, outfile)
