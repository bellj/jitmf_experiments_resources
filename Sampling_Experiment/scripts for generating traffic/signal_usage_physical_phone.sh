# pixel 3xl physical
export PATH=${PATH}:~/Android/Sdk/platform-tools/
adb  shell monkey -p org.thoughtcrime.securesms -c android.intent.category.LAUNCHER 1 # launch Telegram
sleep 5
adb  shell input tap 850 326 # Pick chat - used for telegram SP

# adb  shell input tap 886 2694
sleep 3 # simulate load
adb  shell input keyboard text "$1" # input text
sleep 2 
adb  shell input tap 994 1220 # press send  
sleep 1
adb  shell input tap 256 2173 # minimize
sleep 2
adb shell input keyevent 4 #back
sleep 2
adb shell input keyevent 3 #home
sleep 2
