var LOG_DIR = "jitmflogs";
var APP = "telegram";
var TYPE = "native";
var EVENT = "Telegram Message Present";
const trigger_points = ["send","recv"]; // "send" -> CP trigger point | "recv" -> SP trigger point

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance) {
    var object = ''
    try {
        var messageObject = Java.cast(instance, Java.use("org.telegram.messenger.MessageObject"))
        var text = Java.cast(messageObject.messageText.value, Java.use("java.lang.CharSequence"));

        var messageOwner = Java.cast(messageObject.messageOwner.value, Java.use("org.telegram.tgnet.TLRPC$Message"));
        var fromId = messageOwner.from_id.value
        var peeruser = Java.cast(messageOwner.to_id.value, Java.use("org.telegram.tgnet.TLRPC$TL_peerUser"));

        var to_username='';
        var to_phone='';
        var from_username='';
        var from_phone='';
        Java.choose("org.telegram.messenger.MessagesController", {
            onMatch: function (instance) {
                to_username = '';
                to_phone = '';
                var TLRPCUser = Java.use("org.telegram.tgnet.TLRPC$TL_user");    
                var Integer = Java.use("java.lang.Integer");
                var intInstance = Integer.valueOf(peeruser.user_id.value);
                
                var to_user = Java.cast(instance.getUser(intInstance), TLRPCUser);
                to_username = to_user.username.value;
                to_phone = to_user.phone.value; // TO GET TO AND FROM RENAMING MIDDLE OF "TO"

                intInstance = Integer.valueOf(fromId);
                var from_user = Java.cast(instance.getUser(intInstance), TLRPCUser);
                from_username = from_user.username.value;
                from_phone = from_user.phone.value; // TO GET TO AND FROM RENAMING MIDDLE OF "TO"

                if(to_username.length>0){
                    return 'stop';
                }
            },
            onComplete: function () { }
        });
        object = '{"date": "' + messageOwner.date.value + '", "message_id": "' + messageOwner.id.value + '", "text": "' + text + '", "to_id": "' + peeruser.user_id.value+ '", "to_name": "' + to_username +'", "to_phone": "' + to_phone  + '", "from_id": "' + fromId+ '", "from_name": "' + from_username +'", "from_phone": "' + from_phone  + '"}'
        
    } catch (err) { }
    
    return object
}

function getMessageObjects(time,TP) {
    var klass = "org.telegram.messenger.MessageObject"
    Java.choose(klass, {
        onMatch: function (instance) {
            var object = parseObject(instance)
            if (object != '') {
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": '
                jitmfLog += object + '}\n'
            }

        }, onComplete: function () {
            complete = true
        }
    });
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + TP + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(online){
        getMessageObjects(time, TP)
    } else {
        dumpHeap(TP)
    }
    if (complete) {
        putInFile(hookLogName, hookLog)
        if(online){
            putInFile(jitmfLogName, jitmfLog)
        } else {
            putInFile(jitmfHeapLogName, jitmfHeapLog)
        }
    }
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        var predicateFlag = false;
        var samplingFlag = false;
        var dumpFlag = false;

        var rndSecond = null;
        // var tmpFile = getExternalStorageDirectory() + "/" + LOG_DIR +'tmpRandomNumber.txt';

        var methods = trigger_points
        methods.forEach(function (TP) {        
            Interceptor.attach(Module.getExportByName('libc.so', TP), {
                onEnter: function (args) {
                    // predicate for required for both send and recv TP
                    samplingFlag = false;
                    var fd = args[0].toInt32();

                    if (Socket.type(fd) === null)
                        return;
                    if (Socket.type(fd).toString().indexOf('tcp') > -1) {
                        var address = Socket.peerAddress(fd);
                        if (address === null)
                            return;
                        predicateFlag = true
                    }

                    // sampling
                    var d = new Date();
                    var second = d.getSeconds();
                    var minute = d.getMinutes();
                    var totalSeconds = (minute*60) + second;

                    var rndInt = Math.floor(Math.random() * 243) + 1
                    
                    // var value = parseInt(putInFile(tmpFile,"","r"));
            
                    if(rndSecond===null){
                        Log.v(TAG_L,"empty file");
                        rndSecond = rndInt
                        // putInFile(tmpFile,rndInt) //+arguments[0]);
                    }
                    else{
                        if(totalSeconds % rndSecond == 0){
                            samplingFlag = true;
                            rndSecond = rndInt
                            // putInFile(tmpFile,rndInt,"w+")
                        }                    
                    }
                    dumpFlag = samplingFlag && predicateFlag;
                },
                onLeave: function (args) {
                    if (dumpFlag) {
                        dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP)
                    }
                    jitmfHeapLog = ''
                    jitmfLog = ''
                }
            });
        });
    });
});