# # launched and in chat ready to write
# # run cmd: sudo ./adb_monkey_runner_6.1.1.sh "Sending\ text\ message\ 4" .
adb -s $1 shell monkey -p org.thoughtcrime.securesms -c android.intent.category.LAUNCHER 1 # launch Telegram
sleep 5
adb -s $1 shell input tap 490 561 # Pick first chat - used for telegram SP
# adb -s $1 shell input tap 313 745 # Pick second chat - used for telegram CP

adb -s $1 shell input tap 540 2707 # click on text box
sleep 1
adb -s $1 shell input keyboard text "$2" # input text
sleep 2
adb -s $1 shell input tap 1269 2694 # press send
sleep 2
adb -s $1 shell am force-stop org.thoughtcrime.securesms # close app