mkdir -p $3/data_extracted_msg_send/sdcard

mkdir $3/data_extracted_msg_send/data
echo "[${1}] Getting logcat logs..."
# http://www.fortoo.eu/m/page-media/4/Andriotis-2012-1-wifs.pdf
adb  -s ${1} logcat -d -v time -b main>> ${3}/logcat.txt
adb  -s ${1} logcat -d -v time -b events>> ${3}/logcat.txt

adb root

if [[ $2 == *"telegram"* ]]; then
    echo "[${1}] Getting sdcard logs ..."
    adb -s ${1} pull -a /sdcard/Telegram $3/data_extracted_msg_send/sdcard
    echo "[${1}] Getting data logs..."
    adb -s ${1} pull -a /data/data/org.telegram.messenger $3/data_extracted_msg_send/data
    echo "[${1}] Getting /media/0/Android/data logs..."
    adb -s ${1} pull -a /media/0/Android/data/org.telegram.messenger $3/data_extracted_msg_send/data
    echo "[${1}] Getting app usage..."
    adb -s ${1} pull -a /data/system/usagestats/usage-history.xml $3/data_extracted_msg_send/

    if [[ $4 = "process_jitmflogs" ]]
    then
        echo "[${1}] Getting jitmf logs..."
        adb -s ${1} pull -a /sdcard/jitmflogs $3/data_extracted_msg_send/sdcard
    fi
fi

if [[ $2 == *"signal"* ]]; then
    echo "[${1}] Getting sdcard logs ..."
    adb -s ${1} pull -a /sdcard/Android/data/org.thoughtcrime.securesms/cache $3/data_extracted_msg_send/sdcard
    adb -s ${1} pull -a /sdcard/Android/data/org.thoughtcrime.securesms/files $3/data_extracted_msg_send/sdcard
    
    mkdir -p $3/backup_database

    echo "[${1}] Getting backup database..."
    adb shell find "/sdcard/Download" -iname "*.backup" | tr -d '\015' | while read line; do adb pull "$line" $3/backup_database/; done;

    echo "[${1}] Getting data logs..."
    adb -s ${1} pull -a /data/data/org.thoughtcrime.securesms $3/data_extracted_msg_send/data
    echo "[${1}] Getting app usage..."
    adb -s ${1} pull -a /data/system/usagestats/usage-history.xml $3/data_extracted_msg_send/

    if [[ $4 = "process_jitmflogs" ]]
    then
        :
    else
        echo "[${1}] Remove jitmf logs from baseline ..."
        rm -rf $3/data_extracted_msg_send/sdcard/files/jitmflogs
    fi
fi

if [[ $2 == *"pushbullet"* ]]; then
    echo "[${1}] Getting sdcard logs ..."
    adb -s ${1} pull -a /storage/emulated/0/Android/data/com.pushbullet.android $3/data_extracted_msg_send/sdcard
    echo "[${1}] Getting telephony logs ..."
    adb -s ${1} pull -a /data/data/com.android.providers.telephony $3/data_extracted_msg_send/data
    echo "[${1}] Getting data logs..."
    adb -s ${1} pull -a /data/data/com.pushbullet.android $3/data_extracted_msg_send/data
    echo "[${1}] Getting /media/0/Android/data logs..."
    adb -s ${1} pull -a /data/media/0/Android/data/com.pushbullet.android $3/data_extracted_msg_send/data
    echo "[${1}] Getting app usage..."
    adb -s ${1} pull -a /data/system/usagestats/usage-history.xml $3/data_extracted_msg_send/

    if [[ $4 = "process_jitmflogs" ]]
    then
        echo "[${1}] Getting jitmf logs..."
        adb -s ${1} pull -a /sdcard/jitmflogs $3/data_extracted_msg_send/sdcard
    fi
fi