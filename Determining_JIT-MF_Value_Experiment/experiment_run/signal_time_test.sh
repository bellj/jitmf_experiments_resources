#!/bin/bash
# $1 - parent dir to store results in 

emulator=emulator-5554

echo "Pushing the driver $JITMF_PATH/resources/drivers/cp_tg_online_native_hook.js"
adb push $JITMF_PATH/resources/drivers/cp_signal_online_native_hook.js /data/local/tmp/cp_signal_online_native_hook.js

echo "Stopping Telegram"
adb shell am force-stop org.thoughtcrime.securesms

echo "Removing residue JITMF logs"
adb shell rm -rf /sdcard/jitmflogs/*

adb shell dumpsys gfxinfo org.thoughtcrime.securesms reset

for ((i=1; i <= 100; i++));
do
    echo "Generating Confidential_Info $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Telegram Attack"
    ./phone_automation_scripts/send_signal_msg_andstudio_pxl5.sh $emulator "Noise_$string"
    sleep 2
done

 adb shell dumpsys gfxinfo org.thoughtcrime.securesms > ./done/signal_cp_stats.txt


echo "Pushing the driver $JITMF_PATH/resources/drivers/cp_tg_online_native_hook.js"
adb push $JITMF_PATH/resources/drivers/sp_signal_online_native_hook.js /data/local/tmp/cp_signal_online_native_hook.js

echo "Stopping Telegram"
adb shell am force-stop org.thoughtcrime.securesms

echo "Removing residue JITMF logs"
adb shell rm -rf /sdcard/jitmflogs/*

adb shell dumpsys gfxinfo org.thoughtcrime.securesms reset

for ((i=1; i <= 100; i++));
do
    echo "Generating Confidential_Info $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Telegram Attack"
    ./phone_automation_scripts/send_signal_msg_andstudio_pxl5.sh $emulator "Noise_$string"
    sleep 2
done

 adb shell dumpsys gfxinfo org.thoughtcrime.securesms > ./done/signal_sp_stats.txt





# mkdir -p $1

# sleep 10
# mkdir -p $1/nothing_opened/
# echo "Gathering baseline logs and JIT-MF sources..."
# ./pull_files.sh $emulator $1/nothing_opened process_jitmflogs
# adb shell rm -rf /sdcard/jitmflogs/*

# mkdir -p $1/telegram_only_opened/
# adb shell monkey -p org.telegram.messenger -c android.intent.category.LAUNCHER 1 # launch Telegram
# sleep 10
# echo "Gathering baseline logs and JIT-MF sources..."
# sleep 10
# ./pull_files.sh $emulator $1/telegram_only_opened process_jitmflogs
# sleep 10
# adb shell rm -rf /sdcard/jitmflogs/*

# mkdir -p $1/telegram_chat_opened/
# adb shell input tap 313 545 # Pick first chat - used for telegram SP
# sleep 10
# echo "Gathering baseline logs and JIT-MF sources..."
# ./pull_files.sh $emulator $1/telegram_chat_opened process_jitmflogs
# adb shell rm -rf /sdcard/jitmflogs/*

# adb shell am force-stop org.telegram.messenger # close app

# adb shell rm -rf /sdcard/jitmflogs/*