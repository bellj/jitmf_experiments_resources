var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "spying";
var TYPE = "androidsp";

function getTime(file)
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;
	if(file){
		dateTime = date+'_'+time;
	}
    return dateTime
}

function traceMethod(targetClassMethod, value)
{
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
	
	try {
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){ }
	
	console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");
	
	for (var i = 0; i < overloadCount; i++) {
        var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function() {

			var arg0 = arguments[0]

			var publish = false
			
			const Env = Java.use("android.os.Environment") 
			const Debug = Java.use("android.os.Debug")
			var dir = Env.getExternalStorageDirectory().getAbsolutePath() 
		
			var retval = this[targetMethod].apply(this, arguments);
			
			if (value === 'memory'){
				if (targetClassMethod==='android.content.Intent.setAction'){
					try{
						var Intent = Java.use('android.content.Intent')
						var ret = Java.cast(retval,Intent)
						if (arg0 === 'com.google.android.c2dm.intent.RECEIVE' && ret.getAction().toString() === 'com.google.android.c2dm.intent.RECEIVE'){
							publish = true
						}
					}
					catch(err){}
				}
				if (targetClassMethod==='android.content.Intent$1.createFromParcel'){
					try{
						var Intent = Java.use('android.content.Intent')
						var ret = Java.cast(retval,Intent)
						if (ret.getAction().toString() === 'com.google.android.c2dm.intent.RECEIVE' && retval.toString().includes('cmp=com.pushbullet.android/com.google.firebase.iid.FirebaseInstanceIdReceiver')){
							publish = true
						}
					}
					catch(err){}
				}
			} 
		
			if(publish){
				Debug.dumpHprofData(dir+"/"+APP+"_"+targetClassMethod+"_"+SCENARIO+"_"+TYPE+"_"+getTime(true)+".hprof"); 
			}
			return retval;
		}
	}
}

setTimeout(function () {
	Java.perform(function(){
		// traceMethod('com.android.org.conscrypt.NativeCrypto.SSL_read','memory') //too much activity

		//tracing below conditionally

		//data filter(applies to both):  grep -ri '{"type":"sms_changed"'
		traceMethod('android.content.Intent$1.createFromParcel','memory') //"retval": Intent { act=com.google.android.c2dm.intent.RECEIVE flg=0x11000010 pkg=com.pushbullet.android cmp=com.pushbullet.android/com.google.firebase.iid.FirebaseInstanceIdReceiver (has extras) }}
		traceMethod('android.content.Intent.setAction','memory') //{"time": 2020-5-14 8:7:45, "method": android.content.Intent.setAction,"arg[0]": com.google.android.c2dm.intent.RECEIVE, "retval": Intent { act=com.google.android.c2dm.intent.RECEIVE }}
		
	})
}, 0);