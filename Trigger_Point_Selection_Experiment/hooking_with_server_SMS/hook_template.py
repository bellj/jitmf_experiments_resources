import frida
import sys
import time
import signal
# Start frida server:  adb shell "/data/local/tmp/frida-server &"
# python3 hook.py hook_methods.js com.pushbullet.android:background

# get script name from the command line
scriptname = sys.argv[1]
# open script file
fd = open(scriptname, "r")
# get process to be instrumented from command line
procname = sys.argv[2]

# define callback function to receive and output messages
# received from server
def on_message(message, data):
   print(message)
   print(data)

manager = frida.get_device_manager()
devices = manager.enumerate_devices()
for ldevices in devices:
   if ldevices.id =='emulator-5554':
      # get connect to frida server through usb and attach to process
      session = ldevices.attach(procname)
      # create script using script file opened above
      script = session.create_script(fd.read())
      # close file opened above
      fd.close()
      # setup callback using function defined above
      script.on('message', on_message)
      # load script into the process
      script.load()
      # read from stdin to keep script running
      sys.stdin.read()